//Controllers contain the functions and business logic of our Express JS Application
//meaning all the operations it can do will be place in this file

const Task = require("../models/Task");

//controller function for getting all the tasks

module.exports.getAllTask =()=>{
	return Task.find({}).then(result=>{
		return result;
	})
}

module.exports.createTask = (requestBody) => {
	let newTask = new Task({
		name : requestBody.name
	})
	return newTask.save().then((task,error)=>{
		if(error){
			console.log(error);
			return false;
		}else{
			return task;
		}
	})
}

module.exports.deleteTask = (taskId) => {
	return Task.findByIdAndRemove(taskId).then((removedTask,err)=>{
		if(err){
			console.log(err);
			return false;
		}else{
			return removedTask;
		}
	})
}

module.exports.updateTask = (taskId, newContent) =>{
	return Task.findById(taskId).then((result,error)=>{
		if(error){
			console.log(err);
			return false;
		}
		result.name = newContent.name;
		return result.save().then((updatedTask,saveErr)=>{
			if(saveErr){
				console.log(saveErr);
				return false;
			}else{
				return updatedTask;
			}
		})
	})
}

// activity

module.exports.getTask = (taskId) =>{
	return Task.findById(taskId).then((result,error)=>{
		if(error){
			console.log(error);
			return false;
		}else{
			return result;
		}
	})
}

module.exports.updateTaskStatus = (taskId, newStatus)=>{
	return Task.findById(taskId).then((result,error)=>{
		if(error){
			console.log(err);
			return false;
		}
		result.status = newStatus.status;
		return result.save().then((updatedStatus,saveErr)=>{
			if(saveErr){
				console.log(saveErr);
				return false;
			}else{
				return updatedStatus;
			}
		})
	})
}
