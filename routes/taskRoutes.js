const express = require("express");
const router = express.Router(); // makes it easier to create routes

const taskControllers = require("../controllers/taskControllers");
// Routes
//routes are responsible for defining the URI's(uniform resource identifier) that our client accesses and the corresponding controller functions that will be used when a route is accessed

// Route to get all the tasks
// this route expects to receive a GET request at the URL "/tasks"
// the whole URL is at "http://localhost:3001/tasks"
router.get("/",(req,res)=>{
	taskControllers.getAllTask().then(resultFromController=>res.send(resultFromController));
})

router.post("/",(req,res)=>{
	taskControllers.createTask(req.body).then(resultFromController=>res.send(resultFromController));
})

//Route to delete a task
//expects to receive a DELETE request at the URL "tasks/:id"
//the whole URL is http://localhost:3001/tasks/:id

router.delete("/:id",(req,res)=>{
	taskControllers.deleteTask(req.params.id).then(resultFromController=>res.send(resultFromController));
})

router.put("/:id",(req,res)=>{
	taskControllers.updateTask(req.params.id,req.body).then(resultFromController=>res.send(resultFromController));
})


//activity
router.get("/:id",(req,res)=>{
	taskControllers.getTask(req.params.id).then(resultFromController=>res.send(resultFromController));
})

router.put("/:id/complete",(req,res)=>{
	taskControllers.updateTaskStatus(req.params.id,req.body).then(resultFromController=>res.send(resultFromController));
})
//activity


//use "module.exports" to export the router object to use in the index.js
module.exports = router;